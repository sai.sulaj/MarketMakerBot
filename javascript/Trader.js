// -------------- LOCAL DEPENDENCIES -------------- //

const Analysis                        = require('./Analysis.js');
const ConfigVars                      = require('./ConfigVars.js');
const Position                        = require('./Position.js');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const PRICE_INDEX                     = 0;
const VOL_INDEX                       = 1;

// -------------- CLASS BLUEPRINT -------------- //

class Trader {

  constructor(_DataFetcher) {
    this.dataFetcher = _DataFetcher;
    this.term = _DataFetcher.term;
    this.cache = {};
  }

  printPositions() {
    /*
    this.term.tlog('\n'.repeat(1));
    this.term.tlog(`----- POSITIONS -----`);
    for (var ex in this.cache) {
      if (this.cache.hasOwnProperty(ex)) {
        for (var symb in this.cache[ex]) {
          if (this.cache[ex].hasOwnProperty(symb)) {
            for (var pos = 0; pos < this.cache[ex][symb].length; pos++) {
              let position = this.cache[ex][symb][pos];
              let qeueDelta = Analysis.toReadableDuration(Date.now() - position.getQeueTimestamp());
              let enterDelta = position.getEnterTimestamp() !== null ? Analysis.toReadableDuration(Date.now() - position.getEnterTimestamp()) : 'Null';
              this.term.tlog(`----- ${ex} ${symb} - STATUS: ${position.getStatus()} E.P.: ${position.getEnterPrice()} Amount: ${position.getAmount()} Q.D.: ${qeueDelta} E.D.: ${enterDelta}`);
            }
          }
        }
      }
    }
    this.term.tlog(`----- !POSITIONS -----`);
    this.term.tlog('\n'.repeat(1));
    */
   let pPosData = [];
   let pTimData = [];
    for (var ex in this.cache) {
      if (this.cache.hasOwnProperty(ex)) {
        for (var symb in this.cache[ex]) {
          if (this.cache[ex].hasOwnProperty(symb)) {
            for (var pos = 0; pos < this.cache[ex][symb].length; pos++) {
              let position = this.cache[ex][symb][pos];
              let qeueDelta = Analysis.toReadableDuration(Date.now() - position.getQeueTimestamp());
              let enterDelta = position.getEnterTimestamp() !== null ? Date.now() - position.getEnterTimestamp() : null;
              pPosData.push([ex.toUpperCase(), symb.toUpperCase(), position.getStatus().toUpperCase(), position.getEnterPrice().toFixed(2), position.getAmount()]);
              pTimData.push([ex.toUpperCase(), symb.toUpperCase(), enterDelta]);
            }
          }
        }
      }
    }
    this.term.updatePositions(pPosData);
    this.term.updateTimer(pTimData);
  }

  async beginTrading() {
    while(true) {
      this.printPositions();
      let start = Date.now();
      await this.updatePositions();
      let timeDelta = Math.min(Date.now() - start, 3000);
      await new Promise(resolve => setTimeout(resolve, 3000 - timeDelta));
    }
  }

  queryPositions(_Exchange, _Symbol) {
    _Exchange = _Exchange.toLowerCase();
    _Symbol = _Symbol.toLowerCase();

    if (this.cache.hasOwnProperty(_Exchange) && this.cache[_Exchange].hasOwnProperty(_Symbol)) {
      return this.cache[_Exchange][_Symbol];
    }
    return null;
  }

  openPosition(_Exchange, _Symbol, _Amount, _Ask, _Bid, _SpreadDelta) {
    if (ConfigVars.VERBOSE >= 3)
    { this.term.tlog(`:Trader#openPosition -> _Exchange: ${_Exchange.ccxt.name}, _Symbol: ${_Symbol}, _Amount: ${_Amount}, _Ask: ${_Ask}, _Bid: ${_Bid}, _SpreadDelta: ${_SpreadDelta}`); }

    let exchangeName = _Exchange.ccxt.name.toLowerCase();
    _Symbol = _Symbol.toLowerCase();

    let spreadDelta = _SpreadDelta === undefined ? ConfigVars.ENTER_INCREMENT : _SpreadDelta;
    let _this = this;
    let enterPrice = _Bid * (1 + ((_Ask / _Bid) - 1) * spreadDelta);

    this.term.tlog(`----- ENTERING MARKET ${_Symbol} AT ${_Exchange.ccxt.name} WITH ${_Amount} UNITS AT ${enterPrice} -----`);

    if (!this.cache.hasOwnProperty(exchangeName)) {
      this.cache[exchangeName] = {};
    }
    if (!this.cache[exchangeName].hasOwnProperty(_Symbol)) {
      this.cache[exchangeName][_Symbol] = [];
    }

    let precision = _Exchange.ccxt['markets'][_Symbol.toUpperCase()]['precision']['price'];

    let position = new Position(_Symbol, _Amount, _Ask, _Bid, _Exchange.getTradingFee(), precision, _SpreadDelta);
    this.cache[exchangeName][_Symbol].push(position);
  }

  async updatePositions() {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:Trader#updatePositions`); }

    for (var ex in this.cache) {
      if (this.cache.hasOwnProperty(ex)) {

        let exchange = this.dataFetcher.getExchange(ex);
        let balance = (await exchange.fetchBalance());
        if (balance === null || balance === undefined) {
          this.term.tlog(`:Trader#updatePositions -> ERR: Balance is null. Skipping exchange ${ex}...`);
          continue;
        }
        let freeBalance = balance['free'];
        this.term.updateBalance(exchange.ccxt.name, freeBalance);

        for (var symb in this.cache[ex]) {
          if (this.cache[ex].hasOwnProperty(symb) && this.cache[ex][symb].length > 0) {

            let lastOB = exchange.lastOB[symb.toLowerCase()];
            let bidAskDelta = lastOB['asks'][0][PRICE_INDEX] - lastOB['bids'][0][PRICE_INDEX];
            let oldestPosTimestamp = this.fetchOldestPositionTimestamp(ex, symb);
            let orders = await exchange.fetchOrders(symb, oldestPosTimestamp - (3 * 60000));
            if (orders === null) {
              this.term.tlog(`:Trader#updatePositions -> ERR: ORDERS NULL. SKIPPING SYMBOL ${symb} ON ${ex}`);
              continue;
            }

            // Check if any orders have been partially filled.
            let checkTrades = false;
            if (freeBalance[symb.split('/')[0].toUpperCase()] > 0) {
              checkTrades = true;
            }

            let pos = this.cache[ex][symb].length;
            while (pos--) {
              let position = this.cache[ex][symb][pos];

              // Enter qeued positions.
              if (position.getStatus() == 'qeued') {
                // TODO: Figure out amount.
                let id = await exchange.enterMarket(symb, position.getAmount(), position.getEnterPrice());
                if (id !== null) {
                  position.confirm(id);
                  this.term.tlog(`----- ${exchange.ccxt.name} ${symb} MARKET ENTRANCE CONFIRMED -----`);
                } else {
                  this.term.tlog(`----- ${exchange.ccxt.name} ${symb} MARKET ENTRANCE FAILED -----`);
                  this.cache[ex][symb].splice(pos, 1);
                }
                continue;
              }

              let orderIndex = orders.findIndex((item) => {
                return item['id'] == position.getID();
              });
              if (orderIndex !== -1) {
                let order = orders[orderIndex];
                 if (position.getStatus() != 'qeued') {

                  if (position.getStatus() == 'in_market') {
                    let maxEnter = Analysis.calculateMaxBuyPrice(lastOB['asks'][0][PRICE_INDEX], lastOB['bids'][0][PRICE_INDEX], position.getTradingFee());
                    // Min ROI no longer possible.
                    if (maxEnter < position.getEnterPrice()) {
                      this.term.tlog(`----- ${exchange.ccxt.name} ${symb} MIN ROI NO LONGER POSSIBLE, MAX ENTER: ${maxEnter}, ACTUAL: ${position.getEnterPrice()} -----`);
                      await exchange.exitMarket(position.getID());
                    } else {
                      // Position is expired.
                      if (position.isExpired() && !position.isAtMax()) {
                        this.term.tlog(`----- ${exchange.ccxt.name} ${symb} HAS EXPIRED, OPEN FOR ${Analysis.toReadableDuration(Date.now() - position.getEnterTimestamp())} -----`);
                        //await exchange.exitMarket(position.getID());

                        if (lastOB['bids'][0][PRICE_INDEX] + (bidAskDelta * (position.getSpreadDelta() + ConfigVars.SPREAD_INCREMENT)) <= maxEnter) {
                          this.term.tlog(`----- ${exchange.ccxt.name} ${symb} REENTERING AT ${lastOB['bids'][0][PRICE_INDEX] + (bidAskDelta * (position.getSpreadDelta() + ConfigVars.SPREAD_INCREMENT))} -----`);
                          await exchange.exitMarket(position.getID());
                          this.openPosition(exchange, position.getSymbol(),
                            position.getAmount(), position.getAsk(), position.getBid(),
                            position.getSpreadDelta() + ConfigVars.SPREAD_INCREMENT);
                        } else {
                          await exchange.exitMarket(position.getID());
                          
                          let maxSpreadIncrement = maxEnter / position.getAsk() - 1;
                          this.term.tlog(`----- ${exchange.ccxt.name} ${symb} HIT MAX BUY, REENTERING AT ${lastOB['bids'][0][PRICE_INDEX] + (bidAskDelta * maxSpreadIncrement)} -----`);

                          this.openPosition(exchange, position.getSymbol(),
                            position.getAmount(), position.getAsk(), position.getBid(),
                            maxSpreadIncrement);  
                          position.isAtMax(true);
                        }
                      }

                      // No longer most competitive.
                      else if (lastOB['bids'][0][PRICE_INDEX] >= position.getEnterPrice() + position.getSmallestIncrement()) {
                        this.term.tlog(`----- ${exchange.ccxt.name} ${symb} NO LONGER MOST COMPETITIVE, BEST BID: ${lastOB['bids'][0][PRICE_INDEX]}, ENTER PRICE: ${position.getEnterPrice()} -----`);
                        await exchange.exitMarket(position.getID());
                      }
                    }

                    // Filter for unchecked trades.
                    if (order.hasOwnProperty('trades') && order['trades'] !== undefined) {
                      let uncheckedTrades = order['trades'].filter((trade) => {
                        return position.getLastCheckTimestamp() !== null ? trade['timestamp'] >= position.getLastCheckTimestamp() : true;
                      });
  
                      // Add amount filled to total.
                      let amountFilled = 0;
                      for (var i = 0; i < uncheckedTrades.length; i++) {
                        amountFilled += uncheckedTrades[i]['amount'];
                      }
  
                      if (amountFilled != 0) {
                        let sellID = await exchange.__SellFilledOrder(symb, amountFilled, position.getMinSellPrice());
                        if (sellID !== null) {
                          position.addSellOrderID(sellID);
                          this.term.tlog(`----- ${ex} ${symb} SOLD ${amountFilled} AT ${position.getMinSellPrice()} -----`);
                        }
                      }
                    } else if (!order.hasOwnProperty('trades') || (order.hasOwnProperty('trades') && order['trades'] === undefined) && order['amount'] == order['filled']) {
                      let sellID = await exchange.__SellFilledOrder(symb, order['filled'] * (1 - position.getTradingFee()), position.getMinSellPrice());
                      if (sellID !== null) {
                        position.addSellOrderID(sellID);
                        this.term.tlog(`----- ${ex} ${symb} SOLD ${order['filled']} AT ${position.getMinSellPrice()} -----`);
                      } else {
                        // TODO: Handle error.
                        this.term.tlog(`----- ${ex} ${symb} SELL ORDER FAILED -----`);
                      }
                    }
                  } else if (position.getStatus() == 'selling') {
                    
                    // Get relevent sell orders.
                    let releventSellOrders = orders.filter((item) => {
                      return position.getSellOrderIDs().includes(item['id']);
                    });

                    let isComplete = position.getSellOrderIDs().length === releventSellOrders.length;
                    for (var i = 0; i < releventSellOrders.length; i++) {
                      let sellOrder = releventSellOrders[i];
                      if (sellOrder['status'] == 'open') {
                        isComplete = false;

                        // Check unfilled sell orders.
                        if (lastOB['asks'][0][PRICE_INDEX] <= position.getMinSellPrice()) {
                          this.term.tlog(`----- ${exchange.ccxt.name} ${symb} SELL ORDER NOW IMPOSSIBLE, BEST ASK: ${lastOB['asks'][0][PRICE_INDEX]}, SELL PRICE: ${position.getMinSellPrice()} -----`);

                          await exchange.cancelOrder(sellOrder['id'], sellOrder['symbol']);
                          await exchange.__SellFilledOrder(sellOrder['symbol'], sellOrder['remaining'], position.getRapidExitSellPrice());
                        }
                      }
                    }

                    // Delete filled/cancelled buy order with filled sell orders.
                    if (isComplete) {
                      this.term.tlog(`----- ${exchange.ccxt.name} ${symb} ${position.getStatus()}, POSITION SUCCESSFULLY EXITED -----`);
                      this.deletePosition(position.getID());
                    }
                  }

                  // Flag selling.
                  if (order['status'] == 'closed' || order['status'] == 'canceled') {
                    position.selling();
                  }
                }
              } else {
                this.term.tlog(`-----  ERR: ${exchange.ccxt.name} ${symb} POSITION ${JSON.stringify(position, null, 2)} ORDER NOT FOUND -----\n----- ATTEMPTING TO CANCEL... ------`);
                await exchange.exitMarket(position.getID());
              }
            }
          }
        }
      }
    }
  }

  deletePosition(_Id) {
    for (var ex in this.cache) {
      if (this.cache.hasOwnProperty(ex)) {
        for (var symb in this.cache[ex]) {
          if (this.cache[ex].hasOwnProperty(symb)) {
            let index = this.cache[ex][symb].findIndex((position) => {
              return position.getID() == _Id;
            });
            if (index !== -1) {
              this.cache[ex][symb].splice(index, 1);
              return;
            }
          }
        }
      }
    }
  }

  fetchOldestPositionTimestamp(_Exchange, _Symbol) {
    _Exchange = _Exchange.toLowerCase();
    _Symbol = _Symbol.toLowerCase();

    if (this.cache.hasOwnProperty(_Exchange) && this.cache[_Exchange].hasOwnProperty(_Symbol) && this.cache[_Exchange][_Symbol].length > 0) {
      let oldestTimestamp = this.cache[_Exchange][_Symbol][0].getEnterTimestamp();
      for (var i = 0; i < this.cache[_Exchange][_Symbol].length; i++) {
        if (this.cache[_Exchange][_Symbol][i].getEnterTimestamp() < oldestTimestamp) {
          oldestTimestamp = this.cache[_Exchange][_Symbol][i].getEnterTimestamp();
        }
      }
      return oldestTimestamp;
    } else {
      return null;
    }
  }

  isInMarket(_Exchange, _Symbol) {
    _Exchange = _Exchange.toLowerCase();
    _Symbol = _Symbol.toLowerCase();

    if (this.cache.hasOwnProperty(_Exchange) && this.cache[_Exchange].hasOwnProperty(_Symbol) && this.cache[_Exchange][_Symbol].length > 0) {
      for (var i = 0; i < this.cache[_Exchange][_Symbol].length; i++) {
        if (this.cache[_Exchange][_Symbol][i].getStatus() == 'in_market' || this.cache[_Exchange][_Symbol][i].getStatus() == 'qeued') {
          return true;
        }
      }
    }
    return false;
  }
}

module.exports = Trader;