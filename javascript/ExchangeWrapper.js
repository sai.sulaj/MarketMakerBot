// -------------- DEPENDENCIES -------------- //

const _                           = require('lodash');
const Bottleneck                  = require('bottleneck');
const Request                     = require('then-request');
const CCXT                        = require('ccxt');

const Analysis                    = require('./Analysis.js');
const ConfigVars                  = require('./ConfigVars.js');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const PRICE_INDEX                 = 0;
const VOL_INDEX                   = 1;

// -------------- CLASS BLUEPRINT -------------- //

class ExchangeWrapper {

  constructor(_CCXT, _Term) {
    this.term        = _Term;
    this.ccxt        = _CCXT;
    this.tradingFee  = ConfigVars.getExchangeData(_CCXT.name)['trading_fee'];

    this.lastOB = {};

    this.limiter = new Bottleneck({
      maxConcurrent: 1,
      minTime: ConfigVars.getExchangeData(_CCXT.name)['rate_limit']
    });
  }

  getLimiter() {
    return this.limiter;
  }

  getTradingFee() {
    return this.tradingFee;
  }

  // CCXT method wrappers implementing Bottleneck.
  async fetchBalance() {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#fetchBalance`); }
    let _this = this;

    return await this.limiter.schedule(() => {
      return this.ccxt.fetchBalance();
    }).catch((err) => {
      _this.term.tlog(`:ExchangeWrapper#fetchBalance -> BOTTLENECK ERR: ${err}`);
      return null;
    });
  }

  async fetchOrderbook(_Symbol) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#fetchOrderbook -> _Symbol: ${_Symbol}`); }

    _Symbol = _Symbol.toLowerCase();
    let _this = this;

    let resp = await Request("GET", ConfigVars.DATA_VENDOR_URL + 'getOB', {qs: {
      exchange: _this.ccxt.name.toLowerCase(),
      symbol: _Symbol.replace('/', '_'),
      depth: 1
    }});
    resp = JSON.parse(resp.getBody('utf-8'));
    this.lastOB[_Symbol] = resp[0];
    this.term.updateUpdateTimes(this.ccxt.name, _Symbol);
    return resp;
    //this.lastOB[_Symbol] = await this.ccxt.fetchOrderBook(_Symbol, 3);
    //return this.lastOB[_Symbol];
  }

  async fetchOrders(_Symbol, _Since) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#fetchOrders -> _Symbol: ${_Symbol}, _Since: ${_Since}`); }

    let _this = this;
    _Symbol = _Symbol.toUpperCase();

    return await this.limiter.schedule((_symbol, _since) => {
      return _this.ccxt.fetchOrders(_symbol, _since);
    }, _Symbol, _Since).then((resp) => {
      return resp;
    }).catch((err) => {
      _this.term.tlog(`:ExchangeWrapper#fetchOrders -> ${err}`);
      return null;
    });
  }
  // !CCXT method wrappers.

  async enterMarket(_Symbol, _Amount, _Price) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#enterMarket -> _Symbol: ${_Symbol}, _Amount: ${_Amount}, _Price: ${_Price}`); }

    _Symbol = _Symbol.toUpperCase();
    let _this = this;

    return await this.limiter.schedule((_symbol, _amount, _price) => {
      return _this.ccxt.createLimitBuyOrder(_symbol, _amount, _price);
    }, _Symbol, _Amount, _Price).then((resp) => {
      return resp['id'];
    }).catch((err) => {
      _this.term.tlog(`:ExchangeWrapper#enterMarket -> BOTTLENECK ERR: ${JSON.stringify(err, null, 2)}`);
      return null;
    });
  }

  async exitMarket(_Id) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#exitMarket -> Id: ${Id}`); }

    let _this = this;

    await this.limiter.schedule(async (_id) => {
      await _this.ccxt.cancelOrder(_Id);
    }, _Id).then(() => {
    }).catch((err) => {
      _this.term.tlog(`:ExchangeWrapper#exitMarket -> ${JSON.stringify(err, null, 2)}`);
    });
  }

  async updateCache() {
    await this.ongoingTradesCache.updateOngoingTrades(this);
  }

  async __SellFilledOrder(_Symbol, _Amount, _Price) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`:ExchangeWrapper#__SellFilledOrder -> _Symbol: ${_Symbol}, _Amount: ${_Amount}, _Price: ${_Price}`); }

    _Symbol = _Symbol.toUpperCase();

    if (_Amount < this.ccxt.markets[_Symbol]['limits']['amount']['min']) {
      this.term.tlog(`:ExchangeWrapper#__SellFilledOrder -> ${_Amount} too low.`);
      return;
    }

    let _this = this;

    await this.limiter.schedule((_symbol, _amount, _price) => {
      return _this.ccxt.createLimitSellOrder(_symbol, _amount, _price);
    }, _Symbol, _Amount, _Price).then((resp) => {
      let id = resp['id'];
      return id;
    }).catch((err) => {
      _this.term.tlog(`:ExchangeWrapper#__SellFilledOrder -> ERR: ${JSON.stringify(err, null, 2)}`);
      return null;
    });
  }

  /**
   * @dev -> Fetches stored macd.lastVal for specified symbol.
   * @param (str) _Symbol -> Associated symbol.
   * @returns NULLABLE (MACDObject) -> most recent MACD output.
   */
  async getMACD(_Symbol) {
    if (ConfigVars.VERBOSE >= 3) { this.term.tlog(`getMACD -> _Symbol: ${_Symbol}`); }

    _Symbol = _Symbol.toLowerCase();
    
    let resp = await Request("GET", ConfigVars.DATA_VENDOR_URL + 'getMACD', {qs: {
      exchange: this.ccxt.name.toLowerCase(),
      symbol: _Symbol.replace('/', '_'),
      side: 'asks',
      period: ConfigVars.MACD_PERIOD,
      slow_period: ConfigVars.MACD_SLOW_PERIOD,
      fast_period: ConfigVars.MACD_FAST_PERIOD,
      signal_period: ConfigVars.MACD_SIGNAL_PERIOD
    }});
    return JSON.parse(resp.getBody('utf-8'));
  }

  /**
   * @dev -> Returns oldest orderbook cache entry timestamp. Null if cache length is zero.
   * @param (str) _Symbol -> Associated symbol.
   * @returns NULLABLE (int) -> Oldest orderbook cache entry UNIX timestamp in milliseconds.
   */
  async getOldestOrderbookCacheEntryTimestamp (_Symbol) {
    if (ConfigVars.VERBOSE >= 3) {  this.term.tlog(`:ExchangeWrapper#getOldestOrderbookCacheEntryTimestamp -> _Symbol: ${_Symbol}`); }
    _Symbol = _Symbol.toLowerCase();

    let resp = await Request("GET", "http://ec2-35-183-41-62.ca-central-1.compute.amazonaws.com/api/" + 'getStatus');
    resp = JSON.parse(resp.getBody('utf-8'));
    if (resp.hasOwnProperty(this.ccxt.name.toLowerCase()) && resp[this.ccxt.name.toLowerCase()].hasOwnProperty(_Symbol)) {
      return resp[this.ccxt.name.toLowerCase()][_Symbol]['oldestEntryTime'];
    } else {
      return null;
    }
  }
}

module.exports = ExchangeWrapper;