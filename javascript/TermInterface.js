const Request = require('request'),
      B = require('blessed'),
      C = require('blessed-contrib')

const PRICE_INDEX   = 0;
const VOL_INDEX     = 1;

const MACD_INDEX      = 0;
const SPREAD_INDEX    = 1;
const SENTIMENT_INDEX = 2;

const ConfigVars = require('./ConfigVars.js');
const Analysis   = require('./Analysis.js');

class TermInterface {

  constructor(_Screen) {
    this.S = _Screen;
    this.G = new C.grid({rows: 12, cols: 12, screen: this.S});

    this.exchanges = null;
    this.symbols = null;
    this.activeExchange = null;
    this.activeSymbol = null;
    this.recentExchange = null;
    this.recentSymbol = null;
    this.updateTimes = {};
    this.updateTimesCache = {};

    this.S.key('q', function() {
      process.exit(0);
    });
  }

  updateBalance(_Exchange, _Bal) {
    let pdata = []
    for (var key in _Bal) {
      if (_Bal.hasOwnProperty(key)) {
        if (_Bal[key] !== 0) {
          pdata.push([_Exchange, key, _Bal[key]]);
        }
      }
    }
    this.balanceTable.setData({headers: ['EXCHANGE', 'SYMBOL', 'STATUS'], data: pdata});
  }

  updatePositions(_Data) {
    if (this.positionsTable === undefined) { return; }
    if (_Data.length === 0) {
      this.positionsTable.setData({headers: ['EXCHANGE', 'SYMBOL', 'STATUS', 'E.P.', 'VOL'], data: [[]]});
    } else {
      this.positionsTable.setData({headers: ['EXCHANGE', 'SYMBOL', 'STATUS', 'E.P.', 'VOL'], data: _Data});
    }
  }

  tlog(_Msg) {
    if (this.hasOwnProperty('log')) {

      _Msg = _Msg.toString();

      if (_Msg.length <= 60) {
        this.log.log(_Msg);
        return;
      }

      _Msg = _Msg.match(/.{1,60}/g);

      for (var i = 0; i < _Msg.length; i++) {
        this.log.log(_Msg[i]);
      }
    }
  }

  async updateOBChart() {
    if (this.activeExchange === null || this.activeSymbol == null) { return; }
    let _this = this;
    Request.get({url: ConfigVars.DATA_VENDOR_URL + 'getOBGraph', qs: {
      exchange: _this.activeExchange.toLowerCase().replace('.', '_'),
      symbol: _this.activeSymbol.toLowerCase().replace('/', '_'),
      depth: 100
    }}, function(err, resp, body) {
      if (err) { _this.log.log(err); return; }
      body = JSON.parse(body);
      let asks = body.map(item => { return item['asks'][0][PRICE_INDEX]; });
      let bids = body.map(item => { return item['bids'][0][PRICE_INDEX]; });
      let time = body.map(item => { return item['timestamp']; });
      let len = time.length;
      let bidsData = {title: 'BIDS', y: bids, x: time, style: { line: 'green' }}
      let asksData = {title: 'ASKS', y: asks, x: time, style: { line: 'red' }}
      _this.obChart.options.minY = Math.min(...bids) * 0.999;
      _this.obChart.options.maxY = Math.max(...asks) * 1.001;
      _this.obChart.setData([bidsData, asksData]);
    });
  }

  __InitExchangeToggles(_ExArray) {
    this.exchanges = _ExArray;
    let _this = this;
    var list = this.G.set(0, 0, 1, 8, B.listbar, {
      label: 'EXCHANGES',
      mouse: true,
      keys: true,
      interactive: true,
      style: {
        fg: 'white',
        text: 'white',
        selected: {
          bg: 'green',
          fg: 'white'
        }
      }
    });
    this.exchangeToggles = list;
    this.exchangeToggles.setItems(_ExArray.map(entry => { return entry.toUpperCase(); } ));
    this.exchangeToggles.on('select', function(_Ex) {
      _this.recentExchange = _this.activeExchange;
      _this.activeExchange = _Ex.content.split(':')[1];
    });
    for (var i = 0; i < _ExArray.length; i++) {
      this.updateTimesCache[_ExArray[i]] = [...new Array(60)].map(x => 0);
      this.updateTimes[_ExArray[i]] = 0;
    }
  }

  __InitSymbolToggles(_SymbArray) {
    this.symbols = _SymbArray;
    let _this = this;
    var list = this.G.set(1, 0, 1, 8, B.listbar, {
      label: 'SYMBOLS',
      mouse: true,
      keys: true,
      interactive: true,
      style: {
        fg: 'white',
        text: 'white',
        selected: {
          bg: 'green',
          fg: 'white'
        }
      }
    });
    this.symbolsToggles = list;
    this.symbolsToggles.setItems(_SymbArray.map(entry => { return entry.toUpperCase(); }));
    this.symbolsToggles.on('select', function(_Symb) {
      _this.recentSymbol = _this.activeSymbol;
      _this.activeSymbol = _Symb.content.split(':')[1];
      _this.updateOBChart();
    });
  }

  __InitOBChart() {
    var line = this.G.set(2, 0, 8, 7, C.line, {
      style: {
        line: 'red',
        text: 'green',
        baseline: 'black'
      },
      showLegend: false,
      wholeNumbersOnly: false,
      label: 'ORDERBOOK'
    });
    this.obChart = line;
  }

  __InitPositionsTable() {
    var table = this.G.set(0, 8, 3, 4, C.table, {
      keys: true,
      fg: 'green',
      selectedBg: 'black',
      selectedFg: 'green',
      label: 'POSITIONS',
      columnSpacing: 1,
      columnWidth: [13, 13, 13, 13, 13]
    });
    this.positionsTable = table;
  }

  __InitTimer() {
    var timer = this.G.set(10, 6, 2, 2, C.donut, {
      label: 'TIMERS',
      radius: 8,
      arcWidth: 3,
      remainColor: 'black',
      yPadding: 2,
      data: [
        {percent: 0.1, label: '-:--', color: 'green'},
        {percent: 0.1, label: '-:--', color: 'green'}
      ]
    });
    this.timerDonut = timer;
  }

  updateTimer(_Data) {
    if (this.timerDonut === undefined) { return; }
    let pData = [];
    for (var i = 0 ; i < _Data.length; i++) {
      let entry = _Data[0];

      if (this.activeExchange === null || this.activeSymbol === null
        || this.activeExchange.toLowerCase() !== entry[0].toLowerCase()
        || this.activeSymbol.toLowerCase() !== entry[1].toLowerCase())
      { continue; }
      if (pData.length >= 2) {
        break;
      }

      let percDelta = entry[2] !== null ? entry[2] / ConfigVars.ORDER_INCREMENT_DELAY * 100 : 0;
      let absDelta = entry[2] !== null ? Analysis.toReadableDuration(entry[2]) : '-:--';
      pData.push({percent: percDelta, label: `${absDelta}`});
    }
    this.timerDonut.setData(pData);
  }

  __InitBalanceTable() {
    var table = this.G.set(10, 4, 2, 2, C.table, {
      keys: true,
      fg: 'green',
      label: 'BALANCE',
      selectedBg: 'black',
      selectedFg: 'green',
      columnSpacing: 1,
      scrollable: true,
      columnWidth: [10, 10, 10]
    });
    this.balanceTable = table;
    let data = [
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--'],
      ['--------', '----', '--.--']
    ];
    this.balanceTable.setData({headers: ['EXCHANGE', 'SYMBOL', 'AMOUNT'], data: data});
  }

  __InitInterface() {
    let _this = this;
    var box = this.G.set(10, 0, 2, 2, B.box, {});
    var enableTradingCheck = B.checkbox({
      parent: box,
      mouse: true,
      keys: true,
      shrink: true,
      text: 'TRADING',
      style: {
        checked: {
          bg: 'green'
        }
      }
    });
    this.interfaceConfig = {
      tradingToggle: enableTradingCheck
    }
  }

  __InitIndicators() {
    var table = this.G.set(10, 2, 2, 2, C.table, {
      label: 'INDICATORS',
      selectedBg: 'black',
      selectedFg: 'green',
      columnSpacing: 1,
      columnWidth: [18, 15]
    });
    this.indicatorTable = table;
    this.indicatorCache = {headers: ['INDICATOR', 'VALUE'], data: [
      ['MACD', '--.-'],
      ['SPREAD', '--.--%'],
      ['TRADE SENTIMENT', '-----']
    ]};
    this.indicatorTable.setData(this.indicatorCache);
  }

  __InitPW() {
    var table = this.G.set(2, 7, 2, 1, C.table, {
      label: 'PRICE',
      selectedBg: 'black',
      selectedFg: 'green',
      columnSpacing: 1,
      columnWidth: [5, 10]
    });
    this.pwTable = table;
    let data = {headers: ['F', 'V'], data: [
      ['BID', '---.--'],
      ['ASK', '---.--'],
      ['M.B.', '---.--']
    ]};
    this.pwTable.setData(data);
  }

  updatePW(_Ex, _Symb, _B, _A, _MB) {
    if (this.activeExchange === null || this.activeSymbol === null
      || this.activeExchange.toLowerCase() !== _Ex.toLowerCase()
      || this.activeSymbol.toLowerCase() !== _Symb.toLowerCase())
    { return; }
    let data = {headers: ['F', 'V'], data: [
      ['BID', _B.toFixed(2)],
      ['ASK', _A.toFixed(2)],
      ['M.B.', _MB.toFixed()]
    ]};
    this.pwTable.setData(data);
  }

  __InitPriceBar() {
    var bar = this.G.set(4, 7, 6, 1, C.stackedBar, {
      label: 'P DELTA',
      barWidth: 8,
      barSpacing: 0,
      xOffset: 3,
      barBgColor: ['red', 'green'],
      maxValue: 10,
      showLegend: false,
      showText: true
    });
    this.priceBar = bar;
  }

  updatePriceBar(_Ex, _Symb, _MaxBuy, _Bid) {
    if (this.activeExchange === null || this.activeSymbol === null
      || this.activeExchange.toLowerCase() !== _Ex.toLowerCase()
      || this.activeSymbol.toLowerCase() !== _Symb.toLowerCase())
    { return; }

    let min = Math.min(this.obChart.options.minY, _MaxBuy * 0.99);
    let delta = Math.max(_MaxBuy - min, _Bid - min) * 1.01;
    this.priceBar.options.barBgColor = ['blue', _MaxBuy < _Bid ? 'red' : 'green'];
    this.priceBar.options.maxValue = delta;
    let data = _MaxBuy < _Bid ? [[ (_MaxBuy - min), (_Bid - _MaxBuy) ]] : [[ (_Bid - min), (_MaxBuy - _Bid) ]];
    data[0][0] = parseFloat(data[0][0].toPrecision(4)); data[0][1] = parseFloat(data[0][1].toPrecision(4));
    this.priceBar.setData({
      barCategory: [''],
      stackedCategory: [' ', ' '],
      data: data
    });
  }

  setMACD(_Ex, _Symb, _Val) {
    if (this.activeExchange === null || this.activeSymbol === null
        || this.activeExchange.toLowerCase() !== _Ex.toLowerCase()
        || this.activeSymbol.toLowerCase() !== _Symb.toLowerCase())
    { return; }
    this.indicatorCache.data[MACD_INDEX][1] = `${_Val.toFixed(3)}`;
    this.indicatorTable.setData(this.indicatorCache);
  }

  setSpread(_Ex, _Symb, _Spread) {
    if (this.activeExchange === null || this.activeSymbol === null
      || this.activeExchange.toLowerCase() !== _Ex.toLowerCase()
      || this.activeSymbol.toLowerCase() !== _Symb.toLowerCase())
    { return; }
    this.indicatorCache.data[SPREAD_INDEX][1] = `${_Spread.toFixed(3)}%`;
    this.indicatorTable.setData(this.indicatorCache);
  }

  setSentiment(_Ex, _Symb, _Val) {
    if (this.activeExchange === null || this.activeSymbol === null
      || this.activeExchange.toLowerCase() !== _Ex.toLowerCase()
      || this.activeSymbol.toLowerCase() !== _Symb.toLowerCase())
    { return; }
    this.indicatorCache.data[SENTIMENT_INDEX][1] = `${_Val ? 'POSITIVE' : 'NEGATIVE'}`;
    this.indicatorTable.setData(this.indicatorCache);
  }

  __InitLogs() {
    var log = this.G.set(3, 8, 7, 4, C.log, {
      label: 'LOGS',
      scrollOnInput: true
    });
    this.log = log;
  }

  __InitUpdatePlot() {
    var updateRateLine = this.G.set(10, 8, 2, 4, C.sparkline, {
      label: 'UPDATES (requests/sec)',
      tags: true,
      style: {fg: 'blue'}
    });
    this.updateRateLine = updateRateLine;
  }

  __UpdateUpdatePlot() {
    let labels = ['', ''];
    let pData = [[], []];
    for (var ei = 0; ei < this.exchanges.length; ei++) {
      let ex = this.exchanges[ei];

      this.updateTimesCache[ex].shift();
      let recent = this.updateTimes[ex] >= Date.now() - 1000;
      this.updateTimesCache[ex].push(recent ? 1 : 0);

      if (this.activeExchange !== null
          && this.activeExchange.toLowerCase() === ex.toLowerCase()) {
        labels[0] = ex.toUpperCase();
        pData[0] = this.updateTimesCache[ex];
      } else if (this.recentExchange !== null
          && this.recentExchange.toLowerCase() === ex.toLowerCase()) {
        labels[1] = ex.toUpperCase();
        pData[1] = this.updateTimesCache[ex];
      }
    }
    this.updateRateLine.setData(labels, pData);
    this.S.render();
  }

  updateUpdateTimes(_Ex, _Symb) {
    this.updateTimes[_Ex] = Date.now();
  }

  init(_ExArray, _SymbArray) {
    let _this = this;
    this.__InitExchangeToggles(_ExArray);
    this.__InitSymbolToggles(_SymbArray)
    this.__InitOBChart();
    this.__InitPriceBar();
    this.__InitPositionsTable();
    this.__InitBalanceTable();
    this.__InitInterface();
    this.__InitIndicators();
    this.__InitLogs();
    this.__InitPW();
    this.__InitTimer();
    this.__InitUpdatePlot();

    setInterval(() => {
      _this.updateOBChart();
    }, 3000);
    setInterval(() => {
      _this.__UpdateUpdatePlot();
    }, 1000);

    this.S.render();
  }
}

module.exports = TermInterface;