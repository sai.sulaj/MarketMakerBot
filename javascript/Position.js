// -------------- LOCAL DEPENDENCIES -------------- //

const Analysis       = require('./Analysis.js');
const ConfigVars     = require('./ConfigVars.js');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const PRICE_INDEX    = 0;
const VOL_INDEX      = 1;

// -------------- CLASS BLUEPRINT -------------- //

class Position {

  constructor(_Symbol, _Amount, _Ask, _Bid, _TradingFee, _Precision, _SpreadDelta) {
    this.status = 'qeued';
    this.qeueTimestamp = Date.now();
    this.spreadDelta = _SpreadDelta == undefined ? ConfigVars.ENTER_INCREMENT : _SpreadDelta;
    this.enterPrice = _Bid + (_Ask - _Bid) * this.spreadDelta;

    // Handle other dumbfuck bots.
    let maxBuy = Analysis.calculateMaxBuyPrice(_Bid, _Ask, _TradingFee);
    if (this.enterPrice > maxBuy) {
      this.enterPrice = maxBuy;
      this.atMax = true;
    }

    this.minSellPrice = Analysis.calculateMinSellPrice(this.enterPrice, _TradingFee);
    this.rapidExitSellPrice = Analysis.calculateRapidExitPrice(this.enterPrice, _TradingFee);
    this.sellOrderIDs = [];
    this.smallestIncrement = Math.pow(10,  -1 * _Precision);
    this.atMax = false;

    this.lastCheckTimestamp = null;
    this.id = null;
    this.enterTimestamp = null;
    this.expirationTimestamp = null;

    this.symbol = _Symbol;
    this.amount = _Amount;
    this.ask = _Ask;
    this.bid = _Bid;
    this.tradingFee = _TradingFee;
  }

  isAtMax(_Bool) {
    if (_Bool === undefined) {
      return this.atMax;
    } else {
      this.atMax = _Bool;
    }
  }

  getSmallestIncrement() {
    return this.smallestIncrement;
  }

  confirm(_Id) {
    this.id = _Id;
    this.status = 'in_market';
    this.enterTimestamp = Date.now();
    this.expirationTimestamp = this.enterTimestamp + ConfigVars.ORDER_INCREMENT_DELAY;
  }

  complete() {
    this.status = 'complete';
  }

  selling() {
    this.status = 'selling';
  }

  getSellOrderIDs() {
    return this.sellOrderIDs;
  }

  addSellOrderID(_Id) {
    this.sellOrderIDs.push(_Id);
    this.lastCheckTimestamp = Date.now();
  }

  getStatus() {
    return this.status;
  }

  getEnterTimestamp() {
    return this.enterTimestamp;
  }

  getRapidExitSellPrice() {
    return this.rapidExitSellPrice;
  }

  getExpirationTimestamp() {
    return this.expirationTimestamp;
  }

  getQeueTimestamp() {
    return this.qeueTimestamp;
  }

  getEnterPrice() {
    return this.enterPrice;
  }

  getMinSellPrice() {
    return this.minSellPrice;
  }

  getLastCheckTimestamp() {
    return this.lastCheckTimestamp;
  }

  getID() {
    return this.id;
  }

  getSymbol() {
    return this.symbol;
  }

  getAmount() {
    return this.amount;
  }

  getAsk() {
    return this.ask;
  }

  getBid() {
    return this.bid;
  }

  getTradingFee() {
    return this.tradingFee;
  }

  getSpreadDelta() {
    return this.spreadDelta;
  }

  isExpired() {
    return this.expirationTimestamp !== null ? Date.now() > this.expirationTimestamp : false;
  }
}

module.exports = Position;