/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Fetches data from exchanges.
 */

// ------------- DEPENDENCIES ------------- //

const CCXT            = require("ccxt");
const DownSampler     = require('downsample-lttb');

// ------------- LOCAL DEPENDENCIES ------------- //

const ConfigVars      = require("./ConfigVars.js");
const ExchangeWrapper = require('./ExchangeWrapper.js');
const Analysis        = require('./Analysis.js');

// ------------- GLOBAL VARS & CONSTANTS ------------- //

const PRICE_INDEX = 0;
const VOL_INDEX   = 1;

// ------------- CLASS BLUEPRINT ------------- //

class DataFetcher {

  constructor(_Term) {
    this.activeExchanges = [];
    this.activeSymbols   = [];
    this.symbolIndex     = 0;
    this.exchangeIndex   = 0;
    this.continueRunning = true;
    this.term = _Term;
  }

  getExchanges() {
    return this.activeExchanges.map(entry => { return entry.ccxt.name; });
  }

  getSymbols() {
    return this.activeSymbols;
  }

  getExchange(_Name) {
    let index = this.activeExchanges.findIndex((ex) => {
      return ex.ccxt.name.toLowerCase() == _Name.toLowerCase();
    });
    if (index !== -1) {
      return this.activeExchanges[index];
    } else {
      return null;
    }
  }

  /**
   * Summary: Loads required exchange and symbol data.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @return {Boolean} true if configuration is valid.
   */
  async init() {
    this.term.tlog(`Initializing DataFetcher...`);
    this.activeSymbols = this.__LoadActiveSymbols();
    this.activeExchanges = await this.__LoadActiveExchanges();
    this.term.tlog(`DataFetcher initialized.`);

    this.printIntroduction(1);
    this.printActiveExchanges(1);
    this.printActiveSymbols(1);

    return this.__ValidateConfig();
  }

  async iterateCachedData(_Callback) {
    if (ConfigVars.VERBOSE >= 3) { this.term.log(`:DataFetcher#iterateCachedData`); }

    for (var exI = 0; exI < this.activeExchanges.length; exI++) {
      let exchange = this.activeExchanges[exI];
      for (var symbI = 0; symbI < this.activeSymbols.length; symbI++) {
        let symbol = this.activeSymbols[symbI];

        await _Callback(exchange, symbol);
      }
    }
  }

  /**
   * Summary: Iterates through each activated exchange and symbol and
   *          downloads data.
   * 
   * @since 0.0.1
   * @access public.
   * 
   * @param {method} _Callback Callback function on delay.
   */
  async runLoop(_Callback) {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#runLoop`); }

     while(this.continueRunning) {
        let iterationStartTime = Date.now();

        try {
          //this.term.tlog(`ExI: ${this.exchangeIndex} | ExName: ${this.activeExchanges[this.exchangeIndex].ccxt.name} | SymbI: ${this.symbolIndex} | SymbName: ${this.activeSymbols[this.symbolIndex]}`);

          let ex = this.activeExchanges[this.exchangeIndex];
          let symb = this.activeSymbols[this.symbolIndex];

          await ex.fetchOrderbook(symb.toUpperCase()).then((OBEntry) => {
            ex.lastOB[symb] = OBEntry[0];
          }).catch((err) => {
            this.term.tlog(err.stack);
          });

          let triggerDelay = this.__IncrementExAndSymbIndices();

          if (triggerDelay) {
            await _Callback();

            let delayVariation = 0;
            let iterationTime = Date.now() - iterationStartTime;
            if (iterationTime < ConfigVars.UPDATE_PERIOD) {
              delayVariation = -iterationTime;
            } else {
              delayVariation = -ConfigVars.UPDATE_PERIOD;
            }

            await new Promise(resolve => setTimeout(resolve, ConfigVars.UPDATE_PERIOD + delayVariation));
          }
          this.term.tlog('\n'.repeat(2));
        } catch(err) {
          this.term.tlog(err.stack);
          this.continueRunning = false;
        }
     }
  }

   /**
    * Symmary: Loads activated symbols from config file.
    * 
    * @since 0.0.1
    * @access private.
    * 
    * @return {Array<String>} Array of symbol names in format: '{BASE}/{QUOTE}',
    *                        lowercase.
    */
  __LoadActiveSymbols() {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__LoadActiveSymbols`); }
  
    let tmpArray = [];
  
    ConfigVars.iterateSymbolToggles((_SymbName, _Status) => {
      if (_Status) {
        tmpArray.push(_SymbName.replace('_', '/').toLowerCase());
      }
    });
  
    //.log(`Symbols loaded.`);
    return tmpArray;
  }

  /**
   * Summary: Retrieves active exchange data from config file, and loads markets.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Array<CCXT>} Array of activated exchanges.
   */
  async __LoadActiveExchanges() {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__LoadActiveExchanges`); }

    let tmpArray = [];
    let _this = this;

    await ConfigVars.iterateExchangeToggles(async (_ExName, _Status) => {
      if (_Status) {
        tmpArray = await _this.__ActivateExchange(tmpArray, _ExName.toLowerCase());
      }
    });

    return tmpArray;
  }

  /**
   * Summary: Validates and loads market for exchange. Automatically
   *          removes exchange if any errors found.
   * 
   * @param {Array<ExchangeWrapper>}   _Array Array to add exchange to.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Array<ExchangeWrapper>}  Passed array with that may have
   *                                     specified exchange in it
   */
  async __ActivateExchange(_Array, _ExchangeName) {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__ActivateExchange -> _Array (length): ${_Array.length}, _ExchangeName: ${_ExchangeName}`); }
  
    let exchangeData = ConfigVars.getExchangeData(_ExchangeName);
    let exchange = new ExchangeWrapper(new CCXT[_ExchangeName]({
      'apiKey': exchangeData['api_key'],
      'secret': exchangeData['api_secret']
    }), this.term);
    if (!this.__AssertExchangeFunctionality(exchange)) {
      this.term.tlog(`:DataFetcher#__ActivateExchange -> WARN: ${exchange.ccxt.name} DOES NO SUPPORT THE REQUIRED FUNCTIONS.\nREMOVING...`);
      return _Array;
    }
    let index = _Array.push(exchange) - 1;
  
    try {
      await _Array[index].ccxt.loadMarkets();
    } catch(err) {
      this.term.tlog(`#activateExchange -> ERROR: COULD NOT INIT ${_Array[index].ccxt.name}:\n${err.stack}\nREMOVING FROM ACTIVE EXCHANGES...`);
      _Array.splice(index, 1);
    }
    return _Array;
  }

  /**
   * @dev -> Asserts that exchange supports the necessary functions for trading.
   * @param (ExchangeWrapper) _Exchange -> Exchange to check.
   * @returns (bool) -> true if exchange supports required functions.
   */
  __AssertExchangeFunctionality(_Exchange) {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__AssertExchangeFunctionality -> _Exchange: ${_Exchange.ccxt.name}`); }

    return _Exchange !== null && _Exchange !== undefined
    && (_Exchange.ccxt.has['fetchBalance'] === true)
    && (_Exchange.ccxt.has['createLimitOrder'] === true)
    && (_Exchange.ccxt.has['cancelOrder'] === true) 
    && (_Exchange.ccxt.has['fetchOrders'] === true || _Exchange.ccxt.has['fetchOrders'] === "emulated");
  }

  /**
   * Summary: Increments exchange and symbol indices so that program iterates through each exchange per
   *          symbol.
   * 
   * @since 0.0.1
   * @access private.
   * 
   * @return {Boolean} True if symbols have been switched.
   */
  __IncrementExAndSymbIndices() {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__IncrementExAndSymbIndices`); }
  
    if (this.exchangeIndex === this.activeExchanges.length - 1) {
      this.exchangeIndex = 0;
      if (this.symbolIndex === this.activeSymbols.length - 1) {
        this.symbolIndex = 0;
      } else {
        this.symbolIndex++;
      }
      return true;
    } else {
      this.exchangeIndex++;
      return false;
    }
  }

  /**
   * @dev -> Checks whether program is configured sufficiently to meaningfully execute.
   *         If not, prints error to console.
   * @returns (bool) -> True if configured correctly, false if otherwise.
   */
  async __ValidateConfig() {
    if (ConfigVars.VERBOSE >= 3) { this.log(`:DataFetcher#__ValidateConfig`); }
  
    // Ensure number of enabled exchanges > 0.
    if (this.activeExchanges.length === 0) {
      this.term.tlog(`:DataFetcher#validateConfig -> ERROR: INSUFFICIENT NUMBER OF EXCHANGES ENABLED. TERMINATING PROGRAM...`);
      return false;
    } 
    // Ensure number of enabled symbols > 0
    else if (this.activeSymbols.length === 0) {
      this.term.tlog(`:DataFetcher#validateConfig -> ERROR: INSUFFICIENT NUMBER OF SYMBOLS ENABLED. TERMINATING PROGRAM...`);
      return false;
    }
    // Ensure API keys are valid.
    for (var i = 0; i < this.activeExchanges.length; i++) {
      try {
        await this.activeExchanges[i].fetchBalance();
      } catch(err) {
        this.term.tlog(`#validateConfig -> ERROR: AUTHENTICATION ERROR FOR EXCHANGE ${this.activeExchanges[i].ccxt.name}\n${err.stack}\nTERMINATING PROGRAM...`);
        return false;
      }
    }
    return true;
  }

  /**
   * @dev -> Prints active symbols that are being queried on the markets.
   * @param (int) _NumLines -> Number of blank linkes to print under text.
   */
  printActiveSymbols(_NumLines) {
    this.term.tlog(`ACTIVE SYMBOLS:`);
    for (var i = 0; i < this.activeSymbols.length; i++) {
      this.term.tlog(this.activeSymbols[i]);
    }
    this.printBlankLines(_NumLines);
  }

  /**
   * @dev -> Prints active exchanges that are being queried.
   * @param (int) _NumLines -> Number of blank linkes to print under text.
   */
  printActiveExchanges(_NumLines) {
    this.term.tlog(`ACTIVE EXCHANGES: `);
    for (var i = 0; i < this.activeExchanges.length; i++) {
      this.term.tlog(this.activeExchanges[i].ccxt.name);
    }
    this.printBlankLines(_NumLines)
  }

  /**
   * @dev -> Prints introduction to program with version number.
   * @param (int) _NumLines -> Number of blank lines to print under introduction.
   */
  printIntroduction(_NumLines) {
    this.term.tlog(' ------------------------------------------ ');
    this.term.tlog('/    COGNISYNTH CAPITAL MARKET-MAKING BOT  \\');
    this.term.tlog(`\\             VERSION: ${'2.0.0'}               /`);
    this.term.tlog(' ------------------------------------------ ')
    this.printBlankLines(_NumLines);
  }

  printBlankLines(_NumLines) {
    if (_NumLines > 0) {
      this.term.tlog('\n'.repeat(_NumLines));
    }
  }
}

module.exports = DataFetcher;