// -------------- DEPENDENCIES -------------- //

const _                           = require('lodash');

const ConfigVars                  = require('./ConfigVars.js');

// -------------- CONSTANTS & GLOBAL VARS -------------- //

const PRICE_INDEX                 = 0;
const VOL_INDEX                   = 1;

// -------------- CLASS BLUEPRINT -------------- //

class Analysis {
  
  /**
   * @dev -> Calculates ROI assuming entry and eventual exit of market.
   * @param (float) _AskPrice -> Associated ask price.
   * @param (float) _BidPrice -> Associated bid price.
   * @param OPTIONAL (float) _TradingFee -> Associated trading fee.
   * @returns (float) -> ROI after buying at ask price and selling at bid price.
   */
  static calculateROI (_AskPrice, _BidPrice, _TradingFee) {
    this.__LogFunctionCall('calculateROI', `:Analysis#calculateROI -> _AskPrice: ${_AskPrice}, _BidPrice: ${_BidPrice}, _TradingFee: ${_TradingFee}`);
    let tradingFee = _TradingFee === undefined ? 0 : _TradingFee;
    return ((1 - tradingFee) * _BidPrice / _AskPrice) - 1;
  }
  
  /**
   * @dev -> Calculates market spread between best ask and best bid.
   * @param (JSON) _OrderbookEntry -> Orderbook entry to perform calculation on.
   * @param OPTIONAL (float) _TradingFee -> Associated trading fee.
   */
  static calculateMarketSpread (_OrderbookEntry) {
    this.__LogFunctionCall(`calculateMarketSpread`);
    let lowestAsk   = _OrderbookEntry['asks'][0][PRICE_INDEX];
    let highestBid  = _OrderbookEntry['bids'][0][PRICE_INDEX];
    return (lowestAsk / highestBid) - 1;
  }
  
  /**
   * @dev -> Converts length of time to milliseconds.
   * @param (int) _Days -> Number of days.
   * @param (int) _Hours -> Number of hours.
   * @param (int) _Minutes -> Number of minutes.
   * @param (int) _Seconds -> Number of seconds.
   * @returns (int) -> Number of milliseconds represented by parameters.
   */
  static toMilliseconds (_Days, _Hours, _Minutes, _Seconds) {
    this.__LogFunctionCall(`toMilliseconds`, `_Days: ${_Days}, _Hours: ${_Hours}, _Minutes: ${_Minutes}, _Seconds: ${_Seconds}`);
    if (_Days === undefined || _Hours === undefined || _Minutes === undefined || _Seconds === undefined) {
      throw `ERROR: Invalid parameter -> A parameter is undefined: _Days: ${_Days}, _Hours: ${_Hours}, _Minutes: ${_Minutes}, _Seconds: ${_Seconds}`;
    }

    let conversionConstants = [86400000, 3600000, 60000, 1000];
    let paramsArray = [_Days, _Hours, _Minutes, _Seconds];
    let sum = 0;
    for (var i = 0; i < conversionConstants.length; i++) {
      sum += conversionConstants[i] * paramsArray[i];
    }
    return sum;
  }

  /**
   * @dev -> Converts milliseconds to human readable format.
   * @param (int) _Milliseconds -> Milliseconds to convert.
   * @returns (str) -> Output string in the form "{DAYS} days, {HOURS} hours, {MINUTES} minutes, {SECONDS} seconds".
   */
  static toReadableDuration (_Milliseconds) {
    this.__LogFunctionCall(`toReadableDuration`, `_Milliseconds: ${_Milliseconds}`);
    
    let conversionConstants = [86400000, 3600000, 60000, 1000];
    let outputArray = [0, 0, 0, 0];
    let outputUnits = ['days', 'hours', 'mins', 'secs'];
    
    for (var i = 0; i < conversionConstants.length; i++) {
      let coefficient = Math.floor(_Milliseconds / conversionConstants[i]);
      outputArray[i] = coefficient;
      _Milliseconds -= coefficient * conversionConstants[i];
    }
    
    let outputStr = '';
    for (var i = 0; i < outputArray.length; i++) {
      if (outputArray[i] === 0) { continue; }
      outputStr += `${outputArray[i]} ${outputUnits[i]}`;
      if (i < outputArray.length - 1) { outputStr += ', '; }
    }
    
    return outputStr;
  }
  
  static calculateMaxBuyPrice(_BidPrice, _AskPrice, _TradingFee) {
    this.__LogFunctionCall(`calculateMaxBuyPrice`, `_BidPrice: ${_BidPrice}, _AskPrice: ${_AskPrice}, _TradingFee: ${_TradingFee}`);
    return (_BidPrice + _AskPrice) * Math.pow(1 - _TradingFee, 2) / (2 * (1 + ConfigVars.TARGET_ROI));
  }

  static calculateMinSellPrice(_BuyPrice, _TradingFee) {
    this.__LogFunctionCall(`calculateMinSellPrice`, `_BuyPrice: ${_BuyPrice}, _TradingFee: ${_TradingFee}`);
    return _BuyPrice * (1 + ConfigVars.TARGET_ROI) / Math.pow(1 - _TradingFee, 2);
  }

  static calculateRapidExitPrice(_BuyPrice, _TradingFee) {
    this.__LogFunctionCall(`calculateRapidExitPrice`, `_BuyPrice: ${_BuyPrice}, _TradingFee: ${_TradingFee}`);
    return _BuyPrice / Math.pow(1 - _TradingFee, 2);
  }
  
  static stringIntersect(_ArrayOne, _ArrayTwo) {
    //if (ConfigVars.VERBOSE === 2) { console.log(`:Analysis#stringIntersect`); }

    let indexOne = 0, indexTwo = 0;
    let output = [];

    while (indexOne < _ArrayOne.length && indexTwo < _ArrayTwo.length) {
      if (_ArrayOne[indexOne].toLowerCase() != _ArrayTwo[indexTwo].toLowerCase()) {
        indexTwo += 1;
      } else {
        output.push(_ArrayOne[indexOne]);
        indexOne += 1;
        indexTwo += 1;
      }
    }

    return output;
  }
  
  static __LogFunctionCall(_FunctionName, _Message) {
    /*if (ConfigVars.VERBOSE >= 3) {
      console.log(`:Analysis#${_FunctionName} -> ${_Message == undefined ? '' : _Message}`);
    }*/
  }
}

module.exports = Analysis;