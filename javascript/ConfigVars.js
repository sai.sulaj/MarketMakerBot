/**
 * Author: Saimir Sulaj.
 * Date: May 20, 2018.
 * Purpose: Holds constants and configuration variables.
 */

// ------------- DEFAULT VALS ------------- //

var MIN_SPREAD             = 0.001;
var ORDER_INCREMENT_DELAY  = 1 * 60000;
var TARGET_ROI             = 0.001;
var SPREAD_INCREMENT       = 0.05;
var UPDATE_PERIOD          = 2500;
var OB_DEPTH               = 3;
var VERBOSE                = 2;
var ENTER_INCREMENT        = 0.0001;
var DATA_VENDOR_URL        = process.env.DATA_VENDOR_URL;

var MACD_PERIOD            = 60000;
var MACD_SLOW_PERIOD       = 30;
var MACD_FAST_PERIOD       = 15;
var MACD_SIGNAL_PERIOD     = 2;

// Active exchanges.
EXCHANGE_TOGGLES = {
  POLONIEX: true,
  BITTREX: false
}

EXCHANGE_DATA = {
  POLONIEX: {
    trading_fee: 0.001,
    rate_limit: 800,
    api_key: process.env.POLONIEX_KEY,
    api_secret: process.env.POLONIEX_SECRET,
  },
  BITTREX: {
    trading_fee: 0.0025,
    rate_limit: 1500,
    api_key: process.env.BITTREX_KEY,
    api_secret: process.env.BITTREX_SECRET,
  }
}

BASE_SYMBOLS = ['USDT'];

// Active symbols.
SYMBOL_TOGGLE = {
  DASH_USDT: true,
  XMR_USDT: true
}

class ConfigVars {
  static get MIN_SPREAD() {
    return MIN_SPREAD;
  }
  static set MIN_SPREAD(_Val) {
    MIN_SPREAD = _Val;
  }

  static get ORDER_INCREMENT_DELAY() {
    return ORDER_INCREMENT_DELAY;
  }
  static set ORDER_INCREMENT_DELAY(_Val) {
    ORDER_INCREMENT_DELAY = _Val;
  }

  static get ENTER_INCREMENT() {
    return ENTER_INCREMENT;
  }
  static set ENTER_INCREMENT(_Val) {
    ENTER_INCREMENT = _Val;
  }

  static get TARGET_ROI() {
    return TARGET_ROI;
  }
  static set TARGET_ROI(_Val) {
    TARGET_ROI = _Val;
  }

  static get SPREAD_INCREMENT() {
    return SPREAD_INCREMENT;
  }
  static set SPREAD_INCREMENT(_Val) {
    SPREAD_INCREMENT = _Val;
  }

  static get UPDATE_PERIOD() {
    return UPDATE_PERIOD;
  }
  static get OB_DEPTH() {
    return OB_DEPTH;
  }
  static get VERBOSE() {
    return VERBOSE;
  }
  static get DATA_VENDOR_URL() {
    return DATA_VENDOR_URL;
  }
  static get MACD_PERIOD() {
    return MACD_PERIOD;
  }
  static get MACD_SLOW_PERIOD() {
    return MACD_SLOW_PERIOD;
  }
  static get MACD_FAST_PERIOD() {
    return MACD_FAST_PERIOD;
  }
  static get MACD_SIGNAL_PERIOD() {
    return MACD_SIGNAL_PERIOD;
  }

  static getBaseSymbols() {
    return BASE_SYMBOLS;
  }

  /**
   * Summary: Fetches exchange-specific data.
   * 
   * @param {JSON} _ExName JSON object of exchange data. 
   *                       Nullable.
   */
  static getExchangeData(_ExName) {
    _ExName = _ExName.toUpperCase();
    if (EXCHANGE_DATA.hasOwnProperty(_ExName)) {
      return EXCHANGE_DATA[_ExName];
    } else {
      return null;
    }
  }

  /**
   * Summary: Iterates EXCHANGE_TOGGLE object and calls callback
   *          function with exchange name and enabled status.
   * 
   * @since  1.0.1.
   * @access public.
   * 
   * @param {Function} _Callback Function with exchange name string
   *                             and boolean enabled status as params.
   *                             exchange name is all caps.
   */
  static async iterateExchangeToggles(_Callback) {
    for (var exName in EXCHANGE_TOGGLES) {
      if (EXCHANGE_TOGGLES.hasOwnProperty(exName)) {
        await _Callback(exName, EXCHANGE_TOGGLES[exName]);
      }
    }
  }

  /**
   * Summary: Iterates SYMBOL_TOGGLE object and calls callback
   *          function with symbol name and enabled status.
   * 
   * @since  1.0.1.
   * @access public.
   * 
   * @param {Function} _Callback Function with symbol name string
   *                             and boolean enabled status as params.
   *                             symbol name is all caps, and "/" is 
   *                             replaced with "_".
   */
  static iterateSymbolToggles(_Callback) {
    for (var symbName in SYMBOL_TOGGLE) {
      if (SYMBOL_TOGGLE.hasOwnProperty(symbName)) {
        _Callback(symbName, SYMBOL_TOGGLE[symbName]);
      }
    }
  }

  static getConfigParams() {
    return {
      minSpread: MIN_SPREAD,
      orderWaitDuration: ORDER_INCREMENT_DELAY,
      targetROI: TARGET_ROI,
      spreadIncrement: SPREAD_INCREMENT
    }
  }

  static updateConfigParams(_Val) {
    _Val = JSON.parse(JSON.stringify(_Val));
    if (_Val.hasOwnProperty('minSpread') && _Val['minSpread'] != null) {
      MIN_SPREAD = _Val['minSpread'];
    }
    if (_Val.hasOwnProperty('orderWaitDuration') && _Val['orderWaitDuration'] != null) {
      ORDER_INCREMENT_DELAY = _Val['orderWaitDuration'];
    }
    if (_Val.hasOwnProperty('targetROI') && _Val['targetROI'] != null) {
      TARGET_ROI = _Val['targetROI'];
    }
    if (_Val.hasOwnProperty('spreadIncrement') && _Val['spreadIncrement'] != null) {
      SPREAD_INCREMENT = _Val['spreadIncrement'];
    }
  }
}

module.exports = ConfigVars;
