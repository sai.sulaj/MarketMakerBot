# Market Maker

This is a cryptocurrency trading strategy that fetches parsed data from the Data
Vendor server and makes trades. This strategy can handle multiple cryptos, and
provides a dashboard in the terminal to monitor performance.