
class Poloniex {
  
  constructor(_ApiInfo) {
    this.id = 'poloniex';
    this.name = 'Poloniex';
    this.countries = ['US', 'CA'];
    this.tradingFee = 0.001;
    this.has = {
      'fetchBalance': true,
      'createLimitOrder': true,
      'cancelOrder': true,
      'fetchOrders': true
    }
    if (_ApiInfo && _ApiInfo.hasOwnProperty('apiKey') 
        && _ApiInfo.hasOwnProperty('secret')) {
      this.apiKey = _ApiInfo['apiKey'];
      this.secret = _ApiInfo['secret'];
    }
    
    this.fetchOrderbookCalls = 0;
    this.nextOrderID = 0;
    this.orders = [];
  }
  
  getNextOrderID() {
    this.nextOrderID++;
    return this.nextOrderID;
  }
  
  async loadMarkets() {
    await new Promise(resolve => setTimeout(resolve, 500));
    this.markets = {
      'DASH/USDT': {
        'id': 'dashusdt',
        'symbol': 'DASH/USDT',
        'base': 'DASH',
        'quote': 'USDT',
        'active': true,
        'limits': {
          'amount': {
            'min': 1e-8,
            'max': 1000000000
          },
          'price': {
            'min': 1e-8,
            'max': 1000000000
          },
          'cost': {
            'min': 1e-8,
            'max': 1000000000
          }
        }
      }
    }
    this.symbols = ['DASH/USDT'];
    this.balance = {
      'free': {
        'DASH': 0.00,
        'USDT': 20.00
      },
      'used': {
        'DASH': 0.00,
        'USDT': 0.00
      }
    };
  }
  
  __partialFillBuyOrder(_Id, _Amount, _Price, _TradingFee) {
    let index = this.orders.findIndex(function(entry) {
      return entry['id'] === _Id.toString();
    });
    if (index !== -1) {
      let order = this.orders[index];
      order.filled += _Amount;
      order.remaining -= _Amount;
      
      let symbolArr = order['symbol'].toUpperCase().split('/');
      let base = symbolArr[0];
      let quote = symbolArr[1];
      
      this.balance['free'][base] += _Amount / _Price * (1 - _TradingFee);
      this.balance['used'][quote] -= _Amount;

      if (!order.hasOwnProperty('trades')) {
        order['trades'] = [];
      }

      order['trades'].push({
        timestamp: Date.now(),
        amount: _Amount / _Price * (1 - _TradingFee)
      });
      
      if (order.remaining <= 1e-5) {
        order.status = 'closed';
      }
    }
  }

  __partialFillSellOrder(_Id, _Amount, _Price, _TradingFee) {
    let index = this.orders.findIndex(function(entry) {
      return entry['id'] === _Id.toString();
    });
    if (index !== -1) {
      let order = this.orders[index];
      order.filled += _Amount;
      order.remaining -= _Amount;
      
      let symbolArr = order['symbol'].toUpperCase().split('/');
      let base = symbolArr[0];
      let quote = symbolArr[1];
      
      this.balance['free'][quote] += _Amount * _Price * (1 - _TradingFee);
      this.balance['used'][base] -= _Amount;
      
      if (order.remaining <= 1e-5) {
        order.status = 'closed';
      }
    }
  }
  
  async fetchOrderBook(_Symbol, limit = undefined) {

    _Symbol = _Symbol.toUpperCase();
    this.fetchOrderbookCalls += 1;
    await new Promise(resolve => setTimeout(resolve, 500));
    if (_Symbol === null || _Symbol === undefined) { throw new Error(`Input param "_Symbol" is null or undefined: ${_Symbol}`); }
    if (!this.symbols.includes(_Symbol)) { throw new Error(`Exchange does not support ${_Symbol}`); }
    
    //let output = -Math.cos(this.fetchOrderbookCalls * Math.PI / 50) * 10 + 200;
    //let output = Math.floor(this.fetchOrderbookCalls / 10) + 200;
    let output = 200;

    return [{
      'asks': [
        [(output + 9), 10],
        [(output + 12), 20]
      ],
      'bids': [
        [(output + 6), 5],
        [(output + 3), 30]
      ]
    }];
  }
  
  async fetchBalance() {
    await new Promise(resolve => setTimeout(resolve, 500));
    console.log(this.balance);
    return this.balance;
  }
  
  async createLimitSellOrder(_Symbol, _Amount, _Price) {
    await new Promise(resolve => setTimeout(resolve, 500));
    if (_Symbol === null || _Symbol === undefined ||
        _Amount === null || _Amount === undefined ||
         _Price === null || _Price === undefined) 
    { throw new Error(`All function parameters must exist -> _Symbol: ${_Symbol}, _Amount: ${_Amount}, _Price: ${_Price}`); }
    _Symbol = _Symbol.toUpperCase();
    let base = _Symbol.split('/')[0];
    let quote = _Symbol.split('/')[1];

    if (this.balance['free'][base.toUpperCase()] >= _Amount) {

      this.balance['free'][base.toUpperCase()] -= _Amount;
      this.balance['used'][base.toUpperCase()] += _Amount;

      let id = this.getNextOrderID();
      let partialAmount = _Amount / 2;
      let timerIDs = [];
      for (var i = 1; i <= 2; i++) {
        let timerID = setTimeout(function(_this, _ID, _PartialAmount, _price) {
          _this.__partialFillSellOrder(_ID, _PartialAmount, _price, _this.tradingFee);
        }, (i * 10000), this, id, partialAmount, _Price);
        timerIDs.push(timerID);
      }
      this.orders.push({
        'id': id.toString(),
        'timestamp': Date.now(),
        'lastTradeTimestamp': Date.now(),
        'status': 'open',
        'symbol': _Symbol,
        'type': 'limit',
        'side': 'buy',
        'price': _Price,
        'amount': _Amount,
        'filled': 0,
        'remaining': _Amount,
        'cost': 0,
        'timerIDs': timerIDs
      });
      return {
        'id': id
      }
    } else {
      throw {name: 'InsufficientFunds', message: 'You broke.'};
    }
  }
  
  async createLimitBuyOrder(_Symbol, _Amount, _Price) {
    await new Promise(resolve => setTimeout(resolve, 500));
    if (_Symbol === null || _Symbol === undefined ||
        _Amount === null || _Amount === undefined ||
        _Price === null || _Price === undefined) 
    { throw new Error(`All function parameters must exist -> _Symbol: ${_Symbol}, _Amount: ${_Amount}, _Price: ${_Price}`); }
    _Symbol = _Symbol.toUpperCase();
    let base = _Symbol.split('/')[0];
    let quote = _Symbol.split('/')[1];
    if (this.balance['free'][quote.toUpperCase()] >= _Amount) {

      this.balance['free'][quote.toUpperCase()] -= _Amount;
      this.balance['used'][quote.toUpperCase()] += _Amount;

      let id = this.getNextOrderID();
      let partialAmount = _Amount / 2;
      let timerIDs = [];
      for (var i = 1; i <= 2; i++) {
        let timerID = setTimeout(function(_this, _ID, _PartialAmount, _price) {
          _this.__partialFillBuyOrder(_ID, _PartialAmount, _price, _this.tradingFee);
        }, (i * 15000), this, id, partialAmount, _Price);
        timerIDs.push(timerID);
      }
      this.orders.push({
        'id': id.toString(),
        'timestamp': Date.now(),
        'lastTradeTimestamp': Date.now(),
        'status': 'open',
        'symbol': _Symbol,
        'type': 'limit',
        'side': 'sell',
        'price': _Price,
        'amount': _Amount,
        'filled': 0,
        'remaining': _Amount,
        'cost': 0,
        'timerIDs': timerIDs
      });
      return {
        'id': id
      }
    } else {
      throw {name: 'InsufficientFunds', message: 'You broke.'};
    }
  }
  
  async cancelOrder(_Id, symbol = undefined) {
    await new Promise(resolve => setTimeout(resolve, 500));
    let index = this.orders.findIndex(function(entry) { 
      return entry['id'] === _Id.toString() 
    });
    if (index != -1) {
      this.orders[index]['status'] = 'canceled';
      let side = this.orders[index]['side'];
      let timerIDs = this.orders[index]['timerIDs'];
      for (var i = 0; i < timerIDs.length; i++) {
        clearTimeout(timerIDs[i]);
      }
      if (side === 'sell') {
        let symbolArr = this.orders[index]['symbol'].split('/');
        let quote = symbolArr[1];
        let remaining = this.orders[index]['remaining'];
        this.balance['free'][quote] += remaining;
        this.balance['used'][quote] -= remaining;
      } else {
        let symbolArr = this.orders[index]['symbol'].split('/');
        let base = symbolArr[0];
        let remaining = this.orders[index]['remaining'];
        this.balance['free'][base] += remaining;
        this.balance['used'][base] -= remaining;
      }
    } else {
      throw {name: 'OrderNotFound', message: 'Dumbass'};
    }
  }
  
  async fetchOrders(symbol, since) {
    await new Promise(resolve => setTimeout(resolve, 500));
    /*
    let index = this.orders.findIndex(function(entry) { 
      return entry['id'] === _Id.toString() 
    });
    if (index != -1) {
      return this.orders[index];
    } else {
      throw {name: 'OrderNotFound', message: 'Dumbass'};
    }*/

    symbol = symbol.toUpperCase();
    return this.orders.filter((order) => {
      return order['symbol'].toUpperCase() == symbol;
    });
  }
}

module.exports = Poloniex;
