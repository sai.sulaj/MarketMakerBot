var Poloniex  = require('./poloniex.js');

var CCXT = {
  poloniex: function(_AuthData) {
    return new Poloniex(_AuthData);
  }
}

module.exports = CCXT;