/**
 * Author: Saimir Sulaj.
 * Date: May 11, 2018.
 * Purpose: Trading algorithm intended for the Crypto markets. Built for CogniSynth Capital.
 */

// --------------- DEPENDENCIES --------------- //

const CCXT             = require('ccxt');
const Express          = require('express');
const DownSampler      = require('downsample-lttb');
const Bottleneck       = require('bottleneck');
const Blessed          = require('blessed');
const Twilio           = require('twilio');

require('dotenv').load();

// --------------- LOCAL DEPENDENCIES --------------- //

const Analysis         = require('./javascript/Analysis.js');
const ConfigVars       = require('./javascript/ConfigVars.js');
const DataFetcher      = require('./javascript/DataFetcher.js');
const Trader           = require('./javascript/Trader.js');
const TermInterface    = require('./javascript/TermInterface.js');

// --------------- GLOBAL VARS & CONSTANTS  --------------- //

const PRICE_INDEX      = 0;
const VOL_INDEX        = 1;

const twilioClient     = new Twilio(process.env.TWILIO_SID, process.env.TWILIO_TOK);
var   screen           = Blessed.screen();
var   termInterface    = new TermInterface(screen);
var   dataFetcher      = new DataFetcher(termInterface);
var   trader           = new Trader(dataFetcher);
var   enableTrading    = false;

// --------------- ERROR EVENT --------------- //

process.on('uncaughtException', (err) => {
  twilioClient.messages.create({
    body: `MARKET MAKER BOT ERROR\n ${err}`,
    to: '+17783022093',
    from: '+18677940708'
  });
});

// --------------- MAIN --------------- //

(async () => {
  trader.beginTrading();

  await dataFetcher.init();
  termInterface.init(dataFetcher.getExchanges(), dataFetcher.getSymbols());
  implementTermFunctions(termInterface);
  dataFetcher.runLoop(() => {
    dataFetcher.iterateCachedData(async (exchange, symbol) => {

      let lastOBEntry = exchange.lastOB[symbol];

      if (lastOBEntry === undefined) {
        termInterface.tlog(`${exchange.ccxt.name} - ${symbol} -> LAST OB ENTRY UNDEFINED. SKIPPING...`);
        return;
      }

      let bidPrice = lastOBEntry['bids'][0][PRICE_INDEX],
          askPrice = lastOBEntry['asks'][0][PRICE_INDEX];

      let spread = Analysis.calculateMarketSpread(lastOBEntry);

      let maxBuy = Analysis.calculateMaxBuyPrice(bidPrice, askPrice, exchange.getTradingFee());

      let macdVal = await exchange.getMACD(symbol);

      termInterface.setMACD(exchange.ccxt.name, symbol, macdVal['MACD']);
      termInterface.setSpread(exchange.ccxt.name, symbol, spread*100);
      termInterface.updatePriceBar(exchange.ccxt.name, symbol, maxBuy, bidPrice);
      termInterface.updatePW(exchange.ccxt.name, symbol, bidPrice, askPrice, maxBuy);

      //printStats(exchange.ccxt.name, symbol, spread, macdVal, maxBuy, bidPrice, askPrice);

      if (macdVal !== null && evaluateTrade(macdVal['MACD'], maxBuy, bidPrice)) {
        //termInterface.tlog(`----- POSITIVE MARKET SENTIMENT -----`);
        termInterface.setSentiment(exchange.ccxt.name, symbol, true);
        if (enableTrading) {
          if (!trader.isInMarket(exchange.ccxt.name, symbol)) {
            let enterDelta = (maxBuy - bidPrice) * 0.9 / (askPrice - bidPrice);

            trader.openPosition(exchange, symbol, 0.02, askPrice, bidPrice, enterDelta);
          } else {
            //termInterface.tlog(`----- ${exchange.ccxt.name} ${symbol} ALREADY IN MARKET -----`);
          }
        } else {
          //termInterface.tlog(`----- TRADING DISABLED -----`);
        }
      } else {
        termInterface.setSentiment(exchange.ccxt.name, symbol, false);
      }
    });
  });
})();

// --------------- FUNCTIONS --------------- //

function implementTermFunctions(_Term) {
  _Term.interfaceConfig.tradingToggle.on('check', function() {
    _Term.tlog('---- TRADING ENABLED ----');
    enableTrading = true;
  });
  _Term.interfaceConfig.tradingToggle.on('uncheck', function() {
    _Term.tlog('---- TRADING DISABLED ----');
    enableTrading = false;
  });
}

function evaluateTrade(_MACD, _MaxBuy, _BidPrice) {
  if (ConfigVars.VERBOSE >= 3) { termInterface.tlog(`#evaluateTrade -> _MACD: ${_MACD}, _MaxBuy: ${_MaxBuy}, _BidPrice: ${_BidPrice}`); }
  return _MACD > 0 && _MaxBuy > _BidPrice;
}

/**
 * @dev -> Handles errors.
 * @param (Error) _Error -> Error to handle.
 * @returns (bool) -> Whether or not is recoverable.
 */
function handleError(_Error) {
  if (_Error instanceof CCXT.RequestTimeout) {
    termInterface.tlog(Chalk.red(`#handleError -> WARN: Request timeout.`));
    return true;
  } else if (_Error instanceof CCXT.NetworkError) {
    termInterface.tlog(Chalk.red(`#handleError -> WARN: Network error: ${_Error.message}`));
    return true;
  }
}

function printStats(_ExchangeName, _Symbol, _Spread, _MACD, _MaxBuy, _BidPrice, _AskPrice) {
  termInterface.tlog(`EX: ${_ExchangeName} | SYMB: ${_Symbol} | MACD: ${_MACD === null ? 'Insufficient data' : _MACD['MACD'].toFixed(2)} | SPREAD: ${(_Spread * 100).toFixed(2)}% | MAX_BUY: ${_MaxBuy.toFixed(2)} | BID: ${_BidPrice.toFixed(2)} | ASK: ${_AskPrice.toFixed(2)}`);
}